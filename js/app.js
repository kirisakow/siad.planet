const drawViz = () => {

  var w = 960,
    h = 500,
    zoomFactor = 1.5,
    margin = { top: 0, right: 0, bottom: 0, left: 0 },
    width = w - margin.left - margin.right,
    height = h - margin.top - margin.bottom;

  var color
    = d3.scaleLinear()
      .domain([0, 10])
      .range([d3.color("#ffffff"), d3.color("#B50067")]);

  var svg
    = d3.select("div#chartId")
      .append("div")
      // Container class to make it responsive.
      .classed("svg-container", true)
      .append("svg")
      // Responsive SVG needs these 2 attributes and no width and height attr.
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", `0 0 ${w * zoomFactor} ${h * zoomFactor}`)
      // Class to make it responsive.
      .classed("svg-content-responsive", true)
      .append('g')
      .classed('map', true);

  var projection
    = d3.geoMercator()
      .scale(100 * zoomFactor)
      .translate([w / 2, h / 2]);

  var path = d3.geoPath().projection(projection);

  var formatter = d3.format("");

  var tip
    = d3.tip()
      .attr('class', 'd3-tip')
      .offset([0, 0])
      .html(function (d) {
        var countryInfo = `<strong>Country: </strong><span class="details">${d.properties.name}<br></span>`,
          howManyStudents = `<strong>SIAD Students: </strong><span class="details">${formatter(d.students.length)}<br></span>`,
          studentsList = d.students.map(student => {
            return `<p class="thumbnail-text">
            <img class="thumbnail" src="img/${student.id}" alt="${student.firstname.charAt(0).concat(student.lastname.charAt(0))}">
            ${student.firstname} ${student.lastname}<br/><em>promo ${student.gradyear}</em></p>`
          }).join("");
        return `${countryInfo}${howManyStudents}${studentsList}`;
      });

  svg.call(tip);

  // queue()
  //   .defer(d3.json, "world_countries.json")
  //   .defer(d3.tsv, "world_population.tsv")
  //   .await(ready);

  d3.json("data/world_countries.json")
    .then(function (jsonDataCountries) {
      d3.csv("data/students.csv")
        .then(function (csvDataStudents) {

          const howManyCountries = jsonDataCountries.features.length;
          csvDataStudents.forEach(d => {
            var random = Math.floor(Math.random() * howManyCountries);
            d.country = jsonDataCountries.features[random].id;
          });

          jsonDataCountries.features.forEach((d) => {
            d.students = csvDataStudents.filter(student => d.id === student.country);
          });

          svg.append("g")
            .attr("class", "countries")
            .selectAll("path")
            .data(jsonDataCountries.features)
            .enter().append("path")
            .attr("d", path)
            .style("fill", function (d) { return color(d.students.length); })
            .style('stroke', 'white')
            .style('stroke-width', 1.5)
            .style("opacity", 0.8)
            // tooltips
            .style("stroke", "white")
            .style('stroke-width', 0.3)
            .on('mouseover', function (d) {
              tip.show(d);
              d3.select(this)
                .style("opacity", 1)
                .style("stroke", "white")
                .style("stroke-width", 3);
            }).on('mouseout', function (d) {
              tip.hide(d);
              d3.select(this)
                .style("opacity", 0.8)
                .style("stroke", "white")
                .style("stroke-width", 0.3);
            });

          svg.append("path")
            .datum(topojson.mesh(jsonDataCountries.features, function (a, b) { return a.id !== b.id; }))
            .attr("class", "names")
            .attr("d", path);

          d3.select("body")
            .on('mousemove', function (d) {
              var mouseX = d3.mouse(this)[0],
                mouseY = d3.mouse(this)[1],
                deltaX = mouseX - window.innerWidth / 2,
                deltaY = mouseY - window.innerHeight / 2,
                newX = 0.5 * w - deltaX,
                newY = 0.5 * h - deltaY;
              d3.select("g.countries")
                .attr("transform", `translate(${newX}, ${newY})`);
            });
        });
    });


};


