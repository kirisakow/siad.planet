# Trombinoscope interactif  

### (Interactive facebook for students, alumnae and alumni) 

Demo: https://altdegauche.fr/siad.planet/

Inspiré par [ce planisphère interactif](http://bl.ocks.org/micahstubbs/8e15870eb432a21f0bc4d3d527b2d14f), j'ai eu l'idée de faire un trombinoscope interactif pour le site du master que je fais à l'université de Lille, appelé [Master SIAD](http://mastersiad.univ-lille1.fr/etudiant/parcours-data-sciences), avec ses étudiant⋅e⋅s ancien⋅ne⋅s et actuel⋅le⋅s réparti⋅e⋅s par pays d'origine.

* Côté données, il y a un fichier CSV avec près de 160 individus bidons (des [noms d'animaux et autres bestiaux](https://www.youtube.com/watch?v=yJfh59iEscg) tout simplement, et leurs photos pour certains)
<br>

* Côté script, le code va chercher le fichier CSV, lire les données et répartir les individus parmi les pays du monde. Pour cette première mouture avec des données bidon, la répartition se fait aléatoirement : rechargez la page et vous verrez tout ce petit monde logé dans des pays différents.
<br>

* Un pays qui n'a aucun individu est blanc ; mais plus il a d'individus et plus sa couleur vire vers le bordeau notoire de l'université de Lille (`#B50067`).